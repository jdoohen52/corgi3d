#include "Simple.hpp"

#include "Input.hpp"
#include <algorithm> // std::max
void Simple::_register_methods()
{
	godot::register_method("get_data", &Simple::get_data);
	godot::register_method("change_color", &Simple::change_color);
}

void Simple::_init()
{
	data = "Hello World from C++";
}

godot::String Simple::get_data() const
{
	return data;
}

godot::PoolByteArray Simple::change_color(godot::PoolByteArray pic, godot::Color target)
{
	auto size = pic.size() / 3;
	double mean[] = {.0, .0, .0};
	double stddev[] = {.0, .0, .0};
	for (auto i = 0; i < size; i++)
	{
		mean[0] += pic[i * 3];
		mean[1] += pic[i * 3 + 1];
		mean[2] += pic[i * 3 + 2];
	}
	mean[0] /= size;
	mean[1] /= size;
	mean[2] /= size;
	for (auto i = 0; i < size; i++)
	{
		stddev[0] += pow(pic[i * 3] - mean[0], 2);
		stddev[1] += pow(pic[i * 3 + 1] - mean[1], 2);
		stddev[2] += pow(pic[i * 3 + 2] - mean[2], 2);
	}
	stddev[0] /= sqrt(stddev[0] / size);
	stddev[1] /= sqrt(stddev[1] / size);
	stddev[2] /= sqrt(stddev[2] / size);
	// std::cout << pic[0] << std::endl;

	godot::PoolByteArray ret(pic);
	for (auto i = 0; i < size; i++)
	{
		if (pic[i * 3] - pic[i * 3 + 2] > 0.3 * 255)
		{
			ret.set(i * 3, (uint8_t)(255 * std::max(0., std::min(1., (pic[i * 3] - mean[0]) * (target.r / mean[0]) + target.r))));
			ret.set(i * 3 + 1, (uint8_t)(255 * std::max(0., std::min(1., (pic[i * 3 + 1] - mean[1]) * (target.g / mean[1]) + target.g))));
			ret.set(i * 3 + 2, (uint8_t)(255 * std::max(0., std::min(1., (pic[i * 3 + 2] - mean[2]) * (target.b / mean[2]) + target.b))));
		}
	}
	return ret;
	// std::cout << pic[0] << std::endl;
}

void SimpleSprite::_register_methods()
{
	godot::register_method("_process", &SimpleSprite::_process);
}

void SimpleSprite::_init()
{
	godot::Godot::print("Wheeeeey");
}

void SimpleSprite::_process(double delta)
{
	godot::Vector2 input_dir(0, 0);

	if (godot::Input::get_singleton()->is_action_pressed("ui_right"))
	{
		input_dir.x += 1;
	}
	if (godot::Input::get_singleton()->is_action_pressed("ui_left"))
	{
		input_dir.x -= 1;
	}
	if (godot::Input::get_singleton()->is_action_pressed("ui_down"))
	{
		input_dir.y += 1;
	}
	if (godot::Input::get_singleton()->is_action_pressed("ui_up"))
	{
		input_dir.y -= 1;
	}

	set_position(get_position() + input_dir.normalized() * delta * 300);
}
