tool
extends "res://addons/procedural_maze/maze_multimesh.gd"

func _ready():
	get_node("/root/Player").translation = Vector3(0, 0, 0)
	get_node("/root/Player").reset_eat_count()
	get_node("/root/Player").set_length(1)

func instanciate_objects():
	var objects = [{"type": "banana", "x":0, "y":1},
		{"type": "apple", "x":1, "y":0},
		{"type": "avocado", "x":0, "y":2},
		{"type": "lime", "x":2, "y":0},
		{"type": "lemon", "x":0, "y":6},
		{"type": "orange", "x":0, "y":7},
		{"type": "cucumber", "x":0, "y":8}
	]
#	var time_in_seconds = 10
#	yield(get_tree().create_timer(time_in_seconds), "timeout")
	if OS.has_feature('JavaScript'):
		JavaScript.eval("startNewMazeGame()")
		var time_in_seconds = 3
		while true:
			yield(get_tree().create_timer(time_in_seconds), "timeout")
			var corgi_str = JavaScript.eval("localStorage.getItem('fruit')")
			if typeof(corgi_str) == TYPE_STRING:
				var p = JSON.parse(corgi_str)
				if typeof(p.result) == TYPE_ARRAY:
					objects = p.result
					break
	else:
		print("The JavaScript singleton is NOT available")
		
	for o in objects:
		var object
		print(o)
		if o.type == "apple":
			object = preload("res://world/maze/apple.tscn").instance()
		elif o.type == "banana":
			object = preload("res://world/maze/banana.tscn").instance()
		elif o.type == "avocado":
			object = preload("res://world/maze/avocado.tscn").instance()
		elif o.type == "lime":
			object = preload("res://world/maze/lime.tscn").instance()
		elif o.type == "lemon":
			object = preload("res://world/maze/lemon.tscn").instance()
		elif o.type == "cucumber":
			object = preload("res://world/maze/cucumber.tscn").instance()
		else:
			object = preload("res://world/maze/orange.tscn").instance()
			
		object.translation = corridor_width*Vector3(o.x, 1.25/corridor_width, o.y)
		object.pos = o
		add_child(object) 

