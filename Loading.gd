extends Control

const SIMULATED_DELAY_SEC = 0.1

var thread = null
var video_pending = null

onready var progress = $ProgressBar

func free_current_scene():
	# Free current scene.
	get_tree().current_scene.free()
	get_tree().current_scene = null

func _thread_load(user_data):
	call_deferred("free_current_scene")
	var path = user_data.path
	var video = user_data.video
	var video_player = get_node("LoadingVideo/" + video)
	var video_length = user_data.video_length	
	var player_location = user_data.player_location
	var ril = ResourceLoader.load_interactive(path)
	assert(ril)
	var total = ril.get_stage_count()
	var progress_total = total*video_length*2
	# Call deferred to configure max load steps.
	progress.call_deferred("set_max", progress_total)

	var res = null

	while true: #iterate until we have a resource
		# Update progress bar, use call deferred, which routes to main thread.
		progress.call_deferred("set_value", ril.get_stage()*video_length+video_player.stream_position*total)
		# Simulate a delay.
#		OS.delay_msec(int(SIMULATED_DELAY_SEC * 1000.0))
		# Poll (does a load step).
		var err = ril.poll()
		# If OK, then load another one. If EOF, it' s done. Otherwise there was an error.
		if err == ERR_FILE_EOF:
			# Loading done, fetch resource.
			if video_pending:
				continue
			res = ril.get_resource()
			break
		elif err != OK:
			# Not OK, there was an error.
			print("There was an error loading")
			break

	# Send whathever we did (or did not) get.
	call_deferred("_thread_done", res, video, player_location)


func _thread_done(resource, video, player_location):
	assert(resource)

	# Always wait for threads to finish, this is required on Windows.
	thread.wait_to_finish()

	# Hide the progress bar.
	progress.hide()

	# Instantiate new scene.
	var new_scene = resource.instance()

	# Add new one to root.
	get_tree().root.add_child(new_scene)
	# Set as current scene.
	get_tree().current_scene = new_scene
	Player.translation = player_location
	progress.visible = false	
	get_node("LoadingVideo/" + video).visible = false
	Loading.visible = false
	get_node("LoadingVideo/" + video).disconnect("finished", self, "on_video_finished")


func load_scene(path, video, video_length, player_location):
	thread = Thread.new()
	thread.start( self, "_thread_load", {"path": path, "video": video, "player_location": player_location, "video_length": video_length})
	raise() # Show on top.
	Loading.visible = true
	progress.visible = true
	video_pending = video
	get_node("LoadingVideo/" + video).connect("finished", self, "on_video_finished")
	get_node("LoadingVideo/" + video).visible = true
	get_node("LoadingVideo/" + video).play()
	
func goto_scene(path, video, video_length, player_location=Vector3(0,0,0)):
	load_scene(path, video, video_length, player_location)

func on_video_finished():
	video_pending = null

func current_scene_name():
	return get_tree().current_scene.name
