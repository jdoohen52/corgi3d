extends Spatial


var day_env = load("res://default_env.tres")
var night_env = load("res://night.res")
var current_env = day_env


func _input(event):
	if event.is_action_pressed("toggle_night"):
		if $WorldEnvironment.get_environment() == day_env:
			$WorldEnvironment.set_environment(night_env)
		else:
			$WorldEnvironment.set_environment(day_env)


func _on_Fish_body_entered(body):
	if body is preload("res://Player/Player.gd"):
		Loading.goto_scene("res://world/fight/fight.tscn", "scene_start", 5, Vector3(23, 3, 57))
