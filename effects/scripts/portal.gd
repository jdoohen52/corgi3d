extends Area


onready var p1=get_node("portal/p1")
var iTime=0


func _process(delta):
	iTime+=delta
	p1.material_override.set("shader_param/iTime",iTime)


func _on_portal_body_entered(body):
	if body is preload("res://Player/Player.gd"):
		var current_scene = get_tree().current_scene.name
		if current_scene == "Main":
			Loading.goto_scene("res://maze_game.tscn", "scene_start", 5)
		elif current_scene == "maze_game":
			get_node("/root/Player").submit_eat()
			get_node("/root/Player").reset_original_length()
			Loading.goto_scene("res://Main.tscn", "scene_end", 14)
