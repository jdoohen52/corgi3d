extends KinematicBody
class_name Cat

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var animation_tree: AnimationTree = $AnimationTree
onready var _playback: AnimationNodeStateMachinePlayback = animation_tree["parameters/playback"]
enum CAT_KIND {WHITE, PINK, ORANGE, BLUE, BLACK}
export var kind = CAT_KIND.ORANGE
var CAT_PROP = {
	CAT_KIND.WHITE: {
		life = 100,
		view_range = 7.0,
		attack = 10,
		speed = 3,
		size = 1,
		texture = preload("res://world/fight/white_cat.TGA")
	},
	CAT_KIND.PINK: {
		life = 100,
		view_range = 7.0,
		attack = 10,
		speed = 1,
		size = 1.5,
		texture = preload("res://world/fight/pink_cat.TGA")
	},
	CAT_KIND.ORANGE: {
		life = 100,
		view_range = 7.0,
		attack = 20,
		speed = 1,
		size = 1,
		texture = preload("res://world/fight/orange_cat.TGA")
	},
	CAT_KIND.BLUE: {
		life = 100,
		view_range = 14.0,
		attack = 10,
		speed = 1,
		size = 1,
		texture = preload("res://world/fight/blue_cat.TGA")
	},
	CAT_KIND.BLACK: {
		life = 200,
		view_range = 7.0,
		attack = 10,
		speed = 1,
		size = 1,
		texture = preload("res://world/fight/black_cat.TGA")
	},
}

var life: int

# Called when the node enters the scene tree for the first time.
func _ready():
	life = CAT_PROP[kind].life
	$Mesh_Cat_Abyss.scale *= CAT_PROP[kind].size
	$Mesh_Cat_Abyss/Arm_Cat/Skeleton/Mesh_Cat_Abyss001/Sprite3D/CSGBox.width = life
	var shape = CylinderShape.new()
	shape.height = 4.5
	shape.radius = CAT_PROP[kind].view_range
	$ViewArea/CollisionShape.set_shape(shape)
	set_material()


func set_material():
	var material: SpatialMaterial = $Mesh_Cat_Abyss/Arm_Cat/Skeleton/Mesh_Cat_Abyss001.get_surface_material(0).duplicate()
	material.set_texture(0, CAT_PROP[kind].texture)
	$Mesh_Cat_Abyss/Arm_Cat/Skeleton/Mesh_Cat_Abyss001.material_override = material

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

var state = 'idle'
onready var player = get_node("/root/Player")



func _physics_process(delta):
	if dead:
		return
	if state == 'run':
		move_toward_player()
			

func move_toward_player():
	self.look_at(player.translation, Vector3(0,1,0))
	var v = translation.direction_to(player.translation) * CAT_PROP[kind].speed
	v = move_and_slide(v)


func _on_ViewArea_body_entered(body):
	if dead:
		return
	if state == 'idle' and body == get_node("/root/Player"):
		state = 'run'
		_playback.travel("Run_F_IP")
		get_node("Detect").play()



func _on_ViewArea_body_exited(body):
	if dead:
		return
	if state == 'run' and body == get_node("/root/Player"):
		state = 'idle'
		_playback.travel("Idle_1")
		


func _on_AttackArea_body_entered(body):
	if dead:
		return
	if state != 'attack' and body == get_node("/root/Player"):
		state = 'attack'
		_playback.travel("Attack_1")


func _on_AttackArea_body_exited(body):
	if dead:
		return
	state = 'run'
	_playback.travel("Run_F_IP")


func _on_Claw_body_entered(body):
	if dead:
		return
	if state == 'attack' and body == get_node("/root/Player"):
		get_node("/root/Fight").damage_to_player(CAT_PROP[kind].attack)

var dead = false

func attacked_by_player():
	if dead:
		return
	life -= 50
	$Mesh_Cat_Abyss/Arm_Cat/Skeleton/Mesh_Cat_Abyss001/Sprite3D/CSGBox.width = life
	if life <= 0:
		_playback.travel("Death_Agr_L")
		$Mesh_Cat_Abyss/Arm_Cat.transform.origin -= Vector3(0,20,0)
		dead = true
		get_node("/root/Fight").beat_cat()


