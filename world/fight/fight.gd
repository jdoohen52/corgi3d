extends Spatial

var player_life = 100
var cats = 5

func _ready():
	get_node("/root/Player").set_length(1)
	get_node("/root/Fight/LabelLose").hide()
	get_node("/root/Fight/LabelWin").hide()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func beat_cat():
	cats -= 1
	if cats == 0:
		get_node("/root/Fight/LabelWin").show()
		yield(get_tree().create_timer(3), "timeout")
		get_node('/root/Player/Energy/Hp/TextureProgress').value = 100
		Loading.goto_scene("res://Main.tscn", "scene_end", 14)
		get_node("/root/Player").reset_original_length()

func damage_to_player(amount):
	self.player_life -= amount
	get_node('/root/Player/Energy/Hp/TextureProgress').value = player_life
	if self.player_life <= 0:
		get_node('/root/Player').die()
