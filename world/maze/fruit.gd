extends Area


const APPLE = 0
const AVOCADO = 1
const BANANA = 2
const CUCUMBER = 3
const LEMON = 4
const LIME = 5
const ORANGE = 6

export(Dictionary) var pos

func dog_eat(body, kind):
	if body is preload("res://Player/Player.gd"):
		body.get_node("Drink").play()
		body.eat(kind, self.pos)
		queue_free()

func _on_apple_body_entered(body):
	dog_eat(body, APPLE)

func _on_banana_body_entered(body):
	dog_eat(body, BANANA)

func _on_avocado_body_entered(body):
	dog_eat(body, AVOCADO)

func _on_cucumber_body_entered(body):
	dog_eat(body, CUCUMBER)

func _on_lemon_body_entered(body):
	dog_eat(body, LEMON)

func _on_lime_body_entered(body):
	dog_eat(body, LIME)

func _on_orange_body_entered(body):
	dog_eat(body, ORANGE)
