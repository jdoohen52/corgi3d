extends KinematicBody

#const Simple = preload("res://bin/simple.gdns")
#
#onready var simple_instance = Simple.new()


var direction = Vector3.BACK
var velocity = Vector3.ZERO
var strafe_dir = Vector3.ZERO
var strafe = Vector3.ZERO

var aim_turn = 0

var vertical_velocity = 0
var gravity = 20

var movement_speed = 0
var walk_speed = 3
var run_speed = 10
var acceleration = 6
var angular_acceleration = 7

var roll_magnitude = 17

const APPLE = 0
const AVOCADO = 1
const BANANA = 2
const CUCUMBER = 3
const LEMON = 4
const LIME = 5
const ORANGE = 6
var eat_count = [0,0,0,0,0,0,0]
var eat = []

var original_length = 100

func reset_original_length():
	set_length(original_length)

func reset_eat_count():
	eat_count = [0,0,0,0,0,0,0,0]
	eat = []

func eat(kind, object):
	eat_count[kind]+=1
	eat.push_back(object)
	print(eat_count)
	print(eat)
	
func submit_eat():
	if OS.has_feature('JavaScript'):
		var eat_str = JSON.print(eat)
		JavaScript.eval("localStorage.setItem('eat', '%s')" % eat_str)
		JavaScript.eval("finishMazeGame()")
	else:
		print("JavaScript not available, nothing to submit")
	reset_eat_count()
	
var rest = null
	
func set_length(length):
	var skel = get_node("Mesh/corgi_anim_lowpoly/corgi_anim/Skeleton")
	print("before set length")
	print($Mesh/corgi_anim_lowpoly/corgi_anim/Skeleton/Mesh_Corgi002.get_aabb().size)
	var b = skel.find_bone("spine_3")
	if rest == null:
		rest = skel.get_bone_rest(b)
		print(rest)	

	var newrest = rest.translated(Vector3(0.0, length/100, 0.0))
	skel.set_bone_rest(b, newrest)	
	print("555 %d" % length)
	print("after set length")
	print(newrest)
	print($Mesh/corgi_anim_lowpoly/corgi_anim/Skeleton/Mesh_Corgi002.get_aabb().size)


func _ready():
	print($Mesh/corgi_anim_lowpoly/corgi_anim/Skeleton/Mesh_Corgi002.get_aabb().size)
#	print(simple_instance.get_data())
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	var corgi = null
	if OS.has_feature('JavaScript'):
		JavaScript.eval("""
			console.log('The JavaScript singleton is available')
		""")
		var corgi_str = JavaScript.eval("localStorage.getItem('corgi')")
		var p = JSON.parse(corgi_str)
		if typeof(p.result) == TYPE_DICTIONARY:
			print(p.result["length"]) # Prints "hello"
			corgi = p.result
		else:
			print("error corgi format") # prints '3.0'

	else:
		print("The JavaScript singleton is NOT available")
	direction = Vector3.BACK.rotated(Vector3.UP, $Camroot/h.global_transform.basis.get_euler().y)

	var length = original_length
	if corgi != null:
		length = corgi["length"]
		original_length = length

	set_length(length)
	
	var face: SpatialMaterial = load("res://Player/M_Corgi.material")
	var texture = face.get_texture(0)
	var image : Image = texture.get_data()
	var image_data = image._get_data()
#
	var target = Color.tomato
	if corgi != null:
		target = Color(corgi["color"])

	print(image_data.data[0])
##	var new_color = simple_instance.change_color(image_data.data, target)
#	new_color = image_data.data
#	print(new_color[0])
#	image_data.data = new_color
#	image._set_data(image_data)
	
	texture.create_from_image(image)
	face.set_texture(0, texture)
	$Mesh/corgi_anim_lowpoly/corgi_anim/Skeleton/Mesh_Corgi002.material_override = face

	
func _input(event):
	if dead:
		return
	if event is InputEventMouseMotion:
		aim_turn = -event.relative.x * 0.015 #animates player with mouse movement while aiming (used in line 104)
	
	if event is InputEventKey: #checking which buttons are being pressed
		if event.as_text() == "W" || event.as_text() == "A" || event.as_text() == "S" || event.as_text() == "D" || event.as_text() == "Space"|| event.as_text() == "J":
			if event.pressed:
				get_node("Status/" + event.as_text()).color = Color("ff6666")
			else:
				get_node("Status/" + event.as_text()).color = Color("ffffff")
		
	if !$AnimationTree.get("parameters/roll/active"): # The "Tap To Roll" system
		if event.is_action_pressed("sprint"):
			if $roll_window.is_stopped():
				$roll_window.start()
				
		if event.is_action_released("sprint"):
			if !$roll_window.is_stopped():
				velocity = Vector3( 0, 1, 0 ) * roll_magnitude
				$roll_window.stop()
				$AnimationTree.set("parameters/roll/active", true)
				$AnimationTree.set("parameters/aim_transition/current", 1)
				$roll_timer.start()
				
	if !$AnimationTree.get("parameters/attack/active"): # The "Tap To Roll" system
		if event.is_action_pressed("attack"):
			if $roll_window.is_stopped():
				$roll_window.start()
				$AnimationTree.set("parameters/attack/active", true)				
				get_node("Bark").play()
				if head_colliding and head_colliding.name.begins_with('cat'):
					head_colliding.attacked_by_player()

var dead = false

func die():
	if !dead: # The "Tap To Roll" system
		dead = true
		get_node("/root/Fight/LabelLose").show()
		if $roll_window.is_stopped():
			$roll_window.start()
			$AnimationTree.set("parameters/death/blend_amount", 1)
			yield(get_tree().create_timer(2.0), "timeout")
			die_finish()


func _physics_process(delta):
	
	if !$roll_timer.is_stopped():
		acceleration = 3.5
	else:
		acceleration = 5
	
	if Input.is_action_pressed("aim"):
		$Status/Aim.color = Color("ff6666")
		if !$AnimationTree.get("parameters/roll/active"):
			$AnimationTree.set("parameters/aim_transition/current", 0)
	else:
		$Status/Aim.color = Color("ffffff")
		$AnimationTree.set("parameters/aim_transition/current", 1)
	
	
	var h_rot = $Camroot/h.global_transform.basis.get_euler().y
	
	if Input.is_action_pressed("forward") ||  Input.is_action_pressed("backward") ||  Input.is_action_pressed("left") ||  Input.is_action_pressed("right"):
		
		direction = Vector3(Input.get_action_strength("left") - Input.get_action_strength("right"),
					0,
					Input.get_action_strength("forward") - Input.get_action_strength("backward"))

		strafe_dir = direction
		
		direction = direction.rotated(Vector3.UP, h_rot).normalized()
		
		if Input.is_action_pressed("sprint") && $AnimationTree.get("parameters/aim_transition/current") == 1:
			movement_speed = run_speed
#			$AnimationTree.set("parameters/iwr_blend/blend_amount", lerp($AnimationTree.get("parameters/iwr_blend/blend_amount"), 1, delta * acceleration))
		else:
			movement_speed = walk_speed
#			$AnimationTree.set("parameters/iwr_blend/blend_amount", lerp($AnimationTree.get("parameters/iwr_blend/blend_amount"), 0, delta * acceleration))
	else:
#		$AnimationTree.set("parameters/iwr_blend/blend_amount", lerp($AnimationTree.get("parameters/iwr_blend/blend_amount"), -1, delta * acceleration))
		movement_speed = 0
		strafe_dir = Vector3.ZERO
		
		if $AnimationTree.get("parameters/aim_transition/current") == 0:
			direction = $Camroot/h.global_transform.basis.z
	
	velocity = lerp(velocity, direction * movement_speed, delta * acceleration)

# warning-ignore:return_value_discarded
	move_and_slide(velocity + Vector3.DOWN * vertical_velocity, Vector3.UP)
	
	if !is_on_floor():
		vertical_velocity += gravity * delta
	else:
		vertical_velocity = 0
	
	
	var rotation_y = 0
	if $AnimationTree.get("parameters/aim_transition/current") == 1:
		rotation_y = lerp_angle($Mesh.rotation.y, atan2(direction.x, direction.z) - rotation.y, delta * angular_acceleration)
		# Sometimes in the level design you might need to rotate the Player object itself
		# - rotation.y in case you need to rotate the Player object
	else:
		rotation_y = lerp_angle($Mesh.rotation.y, $Camroot/h.rotation.y, delta * angular_acceleration)
		# lerping towards $Camroot/h.rotation.y while aiming, h_rot(as in the video) doesn't work if you rotate Player object
	$Mesh.rotation.y = rotation_y
	$CollisionShape.rotation.y = rotation_y
	
	
	strafe = lerp(strafe, strafe_dir + Vector3.RIGHT * aim_turn, delta * acceleration)
	
	$AnimationTree.set("parameters/strafe/blend_position", Vector2(-strafe.x, strafe.z))
	
	var iw_blend = (velocity.length() - walk_speed) / walk_speed
	var wr_blend = (velocity.length() - walk_speed) / (run_speed - walk_speed)

	#find the graph here: https://www.desmos.com/calculator/4z9devx1ky

	if velocity.length() <= walk_speed:
		$AnimationTree.set("parameters/iwr_blend/blend_amount" , iw_blend)
	else:
		$AnimationTree.set("parameters/iwr_blend/blend_amount" , wr_blend)
	
	aim_turn = 0
	
	
#	$Status/Label.text = "direction : " + String(direction)
#	$Status/Label2.text = "direction.length() : " + String(direction.length())
#	$Status/Label3.text = "velocity : " + String(velocity)
#	$Status/Label4.text = "velocity.length() : " + String(velocity.length())



var head_colliding: Node = null


func _on_Head_body_entered(body):
	head_colliding = body


func _on_Head_body_exited(body):
	head_colliding = null

func die_finish():
	print('die finish')
	Loading.goto_scene("res://Main.tscn", "scene_end", 14)
	dead = false
	get_node("/root/Player").reset_original_length()
	get_node('Energy/Hp/TextureProgress').value = 100
	$AnimationTree.set("parameters/death/blend_amount", 0)


var scene_help = {
	Main = """
This is your home, walk around to visit different world.
There is currently two worlds: maze and fighting field. 
Find something unusual to enter these worlds.
""",
	maze_game = """
Collect berries in the maze and find a way out. 
Once you successfully leave the maze, you'll get all berries you collected!
""",
	Fight = """
There are five cats in this world. 
When noticed by them, they'll run and fight with you.
Try your best to beat them!
You'll be transfered back home if they or you are defeated.

Hint: different cats have different skills.
White cat: runs very fast
Pink cat: bigger
Orange cat: high damage per attack
Blue cat: wide alert range
Black cat: extra life
"""
}



func _on_TextureButton_pressed():
	$Help/RichTextLabel.visible = !$Help/RichTextLabel.visible
	var scene = Loading.current_scene_name()
	var help = scene_help[scene]
	$Help/RichTextLabelSceneSpecific.text = help
	$Help/RichTextLabelSceneSpecific.visible = !$Help/RichTextLabelSceneSpecific.visible
	
